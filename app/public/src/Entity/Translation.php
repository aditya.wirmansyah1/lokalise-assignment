<?php

namespace App\Entity;

use App\Repository\TranslationRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TranslationRepository::class)
 */
class Translation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $value;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $trans_key;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $language_iso;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getTransKey(): ?string
    {
        return $this->trans_key;
    }

    public function setTransKey(string $trans_key): self
    {
        $this->trans_key = $trans_key;

        return $this;
    }

    public function getLanguageIso(): ?string
    {
        return $this->language_iso;
    }

    public function setLanguageIso(string $language_iso): self
    {
        $this->language_iso = $language_iso;

        return $this;
    }
}
