<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;

class ApiLoginController extends AbstractController
{
    /**
     * @Route("/login", name="api_login", methods={"POST"})
     */
    public function login(JWTTokenManagerInterface $JWTManager): Response
    {
        if (!$this->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->json([
                'error' => 'Invalid login request: check that the Content-Type header is "application/json".'
            ], 400);
        }
        
        if($this->getUser()) {
            $entityManager = $this->getDoctrine()->getManager();
            $user = $entityManager->getRepository(User::class)
                        ->find( $this->getUser()->getId() );

            if (!$user) {
                throw $this->createNotFoundException(
                    'No user found!'
                );
            }

            $token = $JWTManager->create($user);
            
            $user->setToken($token);
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->json([
                'tokens' => $this->getUser() ? $token : null], 
                Response::HTTP_OK
            );
        } else {
            return $this->json([
                'message' => "Invalid email or password!"], 
                Response::HTTP_OK
            );
        }           
    }
}
